using MassTransit;
using Messages;

namespace MessageBus;

public class EventBus
{
    private readonly IBusControl _busControl;

    public EventBus(IBusControl busControl)
    {
        _busControl = busControl;
    }
    
    public Task Publish<TMessage>(TMessage message) 
        where TMessage : class, IEvent
    {
        return _busControl.Publish(message);
    }
}