using MassTransit;
using Messages;

namespace Consumer;

public class EventOneHandler
    : IConsumer<IEventOne>
{
    public Task Consume(ConsumeContext<IEventOne> context)
    {
        return Console.Out.WriteLineAsync($"Received event one with Text={context.Message.Text}");
    }
}