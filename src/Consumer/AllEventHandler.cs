using MassTransit;
using Messages;

namespace Consumer;

public class AllEventHandler
    : IConsumer<IEvent>
{
    public Task Consume(ConsumeContext<IEvent> context)
    {
        return Console.Out.WriteLineAsync(
            $"Received event of type {context.Message.GetType()} with Text={context.Message.Text}");
    }
}