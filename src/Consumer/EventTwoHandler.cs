using MassTransit;
using Messages;

namespace Consumer;

public class EventTwoHandler
    : IConsumer<IEventTwo>
{
    public Task Consume(ConsumeContext<IEventTwo> context)
    {
        return Console.Out.WriteLineAsync($"Received event two with Text={context.Message.Text}, CreatedOn={context.Message.CreatedOn}");
    }
}