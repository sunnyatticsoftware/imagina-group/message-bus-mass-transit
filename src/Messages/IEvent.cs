namespace Messages;

public interface IEvent
{
    string Text { get; }
}