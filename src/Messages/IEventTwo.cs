namespace Messages;

public interface IEventTwo
    : IEvent
{
    DateTime CreatedOn { get; }
}