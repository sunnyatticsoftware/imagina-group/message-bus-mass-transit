using Consumer;
using MassTransit;

namespace PublishConsumer;

public class BusFactory
{
    private readonly BusConfiguration _busConfiguration;

    public BusFactory(BusConfiguration busConfiguration)
    {
        _busConfiguration = busConfiguration;
    }
    
    public IBusControl CreatePublisherWithMassTransit()
    {
        var busControl = Bus.Factory.CreateUsingRabbitMq(
            cfg =>
            {
                cfg.Host(_busConfiguration.Host, "/", hostConfigurator =>
                {
                    hostConfigurator.Username(_busConfiguration.Username);
                    hostConfigurator.Username(_busConfiguration.Password);
                });
            });

        return busControl;
    }
    
    public IBusControl CreateConsumerWithMassTransit()
    {
        var busControl = Bus.Factory.CreateUsingRabbitMq(
            cfg =>
            {
                cfg.Host(_busConfiguration.Host, "/", hostConfigurator =>
                {
                    hostConfigurator.Username(_busConfiguration.Username);
                    hostConfigurator.Username(_busConfiguration.Password);
                });
                
                cfg.ReceiveEndpoint("event-listener", endpointConfiguration =>
                {
                    endpointConfiguration.Consumer<EventOneHandler>();
                    endpointConfiguration.Consumer<EventTwoHandler>();
                    endpointConfiguration.Consumer<AllEventHandler>();
                });
            });

        return busControl;
    }
}