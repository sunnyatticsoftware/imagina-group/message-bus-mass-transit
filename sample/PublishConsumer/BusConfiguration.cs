namespace PublishConsumer;

public class BusConfiguration
{
    public string Host { get; init; } = "localhost";
    public string Username { get; init; } = "guest";
    public string Password { get; init; } = "guest";

    public static BusConfiguration Default => new();
}