using MessageBus;
using Messages;
using PublishConsumer;

Console.WriteLine("Running sample");

var busConfiguration = BusConfiguration.Default;
var busFactory = new BusFactory(busConfiguration);

await RunPublisher();
await RunConsumer();

async Task RunPublisher()
{
    var busControl = busFactory.CreatePublisherWithMassTransit();
    var source = new CancellationTokenSource(TimeSpan.FromSeconds(5));
    await busControl.StartAsync(source.Token);
    var publisher = new EventBus(busControl);
    for (var i = 0; i < 10; i++)
    {
        await publisher.Publish<IEventOne>(new EventOne {Text = $"I am message number {i + 1}"});
        await publisher.Publish<IEventTwo>(new EventTwo {Text = $"I am message number {i + 1}", CreatedOn = DateTime.Now});
    }

    await busControl.StopAsync();
}

async Task RunConsumer()
{
    var busControl = busFactory.CreateConsumerWithMassTransit();
    var source = new CancellationTokenSource(TimeSpan.FromSeconds(5));
    await busControl.StartAsync(source.Token);
    Console.WriteLine("Press enter to end consumer");
    await Task.Run(Console.ReadLine);
    await busControl.StopAsync();
}

namespace PublishConsumer
{
    class EventOne
        : IEventOne
    {
        public string Text { get; init; } = string.Empty;
    }

    class EventTwo
        : IEventTwo
    {
        public string Text { get; init; } = string.Empty;
        public DateTime CreatedOn { get; init; }
    }
}