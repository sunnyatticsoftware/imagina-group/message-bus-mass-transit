# message-bus-mass-transit

MassTransit es una librería open source que nos abstrae del protocolo de mensajería concreto. 
Aporta la funcionalidad de enviar mensajes, publicar mensajes, sagas, entregas programadas, etc. y además crea automáticamente la topología (colas y endpoints).

También se puede testear fácilmente

MassTransit ofrece un paquete `MassTransit.Abstractions` que se puede utilizar como dependencia en nuestra capa de Aplicación sin necesidad de abstraerlo.

La creación del bus corresponde a la capa de Infraestructura.

Observa como los proyectos Consumer y Publisher solamente dependen de `MassTransit.Abstractions`.

Observa como se puede testear con un Harness sin necesidad de utilizar un transporte concreto.

## RabbitMQ
Ejecuta un contenedor con RabbitMq con UI
``` 
$ docker run --name rabbitmq -p 15672:15672 -p 5672:5672 masstransit/rabbitmq
```
Browse http://localhost:15672 with `guest`/`guest` credentials

## Ejercicio para profundizar
Acude a la documentación oficial de MassTransit y modifica la aplicación de consola de ejemplo para que utilice transporte con ActiveMQ, SQS o Azure Service Bus, y comprueba cómo el servicio de publicación de mensajes
y de consumo de esos mensajes es totalmente agnóstico de la capa de transporte.