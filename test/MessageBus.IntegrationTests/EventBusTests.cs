using Consumer;
using FluentAssertions;
using MassTransit;
using MassTransit.Testing;
using Messages;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;

namespace MessageBus.IntegrationTests;

public class EventBusTests
{
    private readonly InMemoryTestHarness _harness;

    public EventBusTests()
    {
        var provider =
            new ServiceCollection()
                .AddMassTransitInMemoryTestHarness(cfg =>
                {
                    cfg.AddConsumer<EventOneHandler>();
                })
                .BuildServiceProvider(true);

        _harness = provider.GetRequiredService<InMemoryTestHarness>();
    }
    
    [Fact]
    public async Task Then_It_Should_Publish_Message()
    {
        await _harness.Start();
        var sut = new EventBus(_harness.BusControl);
        await sut.Publish(new EventOne {Text = "foo"});
        var isAnyEventOnePublished = await _harness.Published.Any<IEventOne>();
        isAnyEventOnePublished.Should().BeTrue();
        await _harness.Stop();
    }
    
    [Fact]
    public async Task Then_It_Should_Handle_Message()
    {
        await _harness.Start();
        var eventBus = new EventBus(_harness.BusControl);
        await eventBus.Publish(new EventOne {Text = "foo"});
        var isAnyEventOneConsumed = await _harness.Consumed.Any<IEventOne>();
        isAnyEventOneConsumed.Should().BeTrue();
        await _harness.Stop();
    }
    
    class EventOne : IEventOne
    {
        public string Text { get; init; }
    }
}